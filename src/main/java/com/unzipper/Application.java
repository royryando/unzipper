package com.unzipper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Application {

    public static void main (String[] args) {

        String zipsDir = "/home/royryando/workspace/unzipper-data/zip";
        String extractDir = "/home/royryando/workspace/unzipper-data/extracted";
        String datDir = "/home/royryando/workspace/unzipper-data/dat";

        File[] files = new File(zipsDir).listFiles();
        FileInputStream fis;

        byte[] buffer = new byte[1024];

        for (final File file : files) {

            if(file.isDirectory()) {

                File[] files2 = file.listFiles();
                for (final File file2 : files2) {
                    if(file2.isFile()) {
                        String fileName = file2.getName();
                        if(fileName.substring(fileName.length() - 4).equals(".zip")) {

                            try {
                                String fixDir = extractDir + File.separator + file.getName();
                                if(!new File(fixDir).exists()) new File(fixDir).mkdirs();
                                fis = new FileInputStream(file2.getAbsolutePath());
                                ZipInputStream zis = new ZipInputStream(fis);
                                ZipEntry ze = zis.getNextEntry();
                                while(ze != null) {
                                    String fName = ze.getName();
                                    if(fName.substring(fName.length() - 4).equals(".dat")) {
                                        File nFile = new File(fixDir + File.separator + fName);
                                        System.out.println("Unzipping to " + nFile.getAbsolutePath());

                                        FileOutputStream fos = new FileOutputStream(nFile);

                                        int len;
                                        while ((len = zis.read(buffer)) > 0) {
                                            fos.write(buffer, 0, len);
                                        }

                                        fos.close();

                                        /**
                                         * COPY TO .dat BASE FOLDER
                                         */
                                        System.out.println("Moving " + fName + " to .dat base folder");
                                        FileUtils.copyFile(nFile, new File(datDir + File.separator + fName));

                                    }
                                    zis.closeEntry();
                                    ze = zis.getNextEntry();

                                }
                                zis.closeEntry();
                                zis.close();
                                fis.close();

                            } catch(Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            }



        }
    }

    /*private static void unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);

        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;

        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null) {
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                System.out.println("Unzipping to " + newFile.getAbsolutePath());

                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();

                zis.closeEntry();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
            fis.close();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }*/

}
